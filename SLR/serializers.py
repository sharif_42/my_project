from rest_framework import serializers

class DemoSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=20)
    Phone_number = serializers.CharField(max_length=20)
    deposit = serializers.IntegerField()
    deposit_account = serializers.CharField(max_length=20)

