from django.apps import AppConfig
# from django.db.models.signals import post_save
#
# from SLR.models import Public
# from SLR.signals import public_created_message


class SlrConfig(AppConfig):
    name = 'SLR'

    def ready(self):
        import SLR.signals
        #post_save.connect(public_created_message, sender=Public, weak=False)
