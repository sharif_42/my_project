from django.contrib.auth.models import User
from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=100,unique=True)
    age = models.IntegerField()
    total_publication = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    def update_publication(book_name):
        print(book_name)
        author_name = Book.objects.get(book_name=book_name).author.name
        a = Author.objects.get(name=author_name)
        a.total_publication+=1
        a.save()



class Book(models.Model):
    author = models.ForeignKey(Author,on_delete=models.SET_NULL,null=True,blank=True)
    book_name = models.CharField(max_length=100,unique=True)
    page = models.IntegerField(default=0)
    price = models.IntegerField(default=0)

    def __str__(self):
        return self.book_name


class Public(models.Model):
    name = models.CharField(max_length=100,unique=True)
    phone_number = models.CharField(max_length=20)

    def __str__(self):
        return self.name
