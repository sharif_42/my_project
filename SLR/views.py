from django.db import transaction
from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Public
from .serializers import DemoSerializer


class DemoView(APIView):
    def post(self, request, *args, **kwargs):
        print(request.data)
        serializer = DemoSerializer(data=request.data)
        if serializer.is_valid():
            print(serializer.data)
            return Response("success", status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateAuthor(APIView):
    pass


class CreateBook(APIView):
    pass


class CreatePublic(APIView):
    def post(self, request, *args, **kwargs):
        with transaction.atomic():
            public = Public.objects.create(
                name=request.data['name'],
                phone_number=request.data['phone_number']

            )

            if public:
                return Response("Public objects Created Successfully", status=status.HTTP_201_CREATED)
            return Response("Public Objects not Created ", status=status.HTTP_400_BAD_REQUEST)
