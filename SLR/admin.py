from django.contrib import admin

# Register your models here.
from .models import Public,Author,Book
admin.site.register(Author)
admin.site.register(Book)
admin.site.register(Public)
