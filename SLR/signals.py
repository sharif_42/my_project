from django.db.models.signals import post_save
from django.dispatch import receiver

from SLR.models import Public,Book,Author

"""
# 
# @receiver(post_save, sender=Author, weak=False)
# def notify_user(sender, instance, **kwargs):
#     if kwargs['created']:
#         print("an author is created")
# 
# """
@receiver(post_save, sender=Book)
def notify_author(sender, instance, **kwargs):
    if kwargs['created']:
        #print(instance.author)
        print(instance.book_name)
        Author.update_publication(instance.book_name)
        print('A book is published')

@receiver(post_save, sender=Public)
def public_created_message(sender, instance, weak=False, **kwargs):
    if kwargs['created']:
        print(kwargs)
        print("a public instance is created")
